`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: AMK INC
// Engineer: 
// 
// Create Date: 12.06.2018 03:18:52
// Design Name: 
// Module Name: elevator_controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module elevator_controller(
    input [5:0] zadane_pietro, //6-bitowy poziom żadania pięter
    input [5:0] in_act_pietro, //6-bitowy poziom aktualnego pietra
    input clk, //generawania zegara o niskiej częstotliwosci
    input rst, //1-bitowy rst wejścia
    input nadczasowosc, //wejście 1-bitowe wskazujące, że drzwi są otwarte dłużej niż założono
    input przeciazenie, //wejście 1-bitowe, które wskazuje, że masa w windzie jest powyżej normy
    output kierunek, //1-bitowe wyjście wskazujące kierunek windy
    output koniec, //1-bitowe wyjście wskazujące, czy winda jest na piętrze, na którym powinna
    output otwarte_drzwi, //wyjście 1-bitowe wskazujące, że drzwi są otwarte dluzej niz założono
    output przeciazenie_pow, //Wyjście 1-bitowe, które wskazuje, że masa w windzie jest powyżej normy
    output [5:0] out_act_pietro //6-bitowy wyjście, które pokazuje aktualne piętro
    );

    reg r_kierunek; //1 bitowy rejestr podłączony do kierunku wyjścia
    reg r_koniec; //1 bitowy rejestr podłączony do wyjścia zakończenia
    reg r_otwarte_drzwi; //1 bitowy rejestr podłączony do alarmu drzwi podczas wyjścia
    reg r_przeciazenie_pow; //1 bitowy rejestr podłączony do wyjścia ciążenia alarmowego
    reg [5:0] r_out_act_pietro; //6-bitowy rejestr podłączony do wyjścia aktualnego piętra
    reg [12:0] clk_licznik; //licznik zegara niskiej częstotliwość jako 12bitowy parametr rejestru
    reg clk_200; //generuje zegar 200Hz
    reg clk_wyzw; //wyzwalacz zegara generatora
    
    assign kierunek = r_kierunek; //zapisanie danych kierunku windy do r_kierunek
    assign koniec = r_koniec; //zapisanie dzialania windy do r_koniec
    assign otwarte_drzwi = r_otwarte_drzwi; //przekazanie czy drzwi są otwarte do r_otwarte_drzwi
    assign przeciazenie_pow = r_przeciazenie_pow; //przeciążenie
    assign out_act_pietro = r_out_act_pietro; //aktualne piętro
    
    always @(negedge rst) begin //wyzwalanie bloku na malejącej krawędzi sygnału rst
        clk_200 = 1'b0; //resetowanie clk 200hz
        clk_licznik = 0; //res. licznika
        clk_wyzw = 1'b0; //res. wyzwalacza
    end
    
    always @(negedge rst) begin //wyzwalanie bloku na malejącej krawędzi sygnału rst
        clk_200 = 1'b0; //rstowanie clk 200hz
        clk_licznik = 0; //res. licznika
        clk_wyzw = 1'b0; //res. wyzwalacza

        r_koniec = 1'b0; //ustawienie domyślnej wartości na 0
        r_otwarte_drzwi = 1'b0; //to samo
        r_przeciazenie_pow = 1'b0; //to samo
    end
    
    /*
    	generator zegara
        licznik bedzie sie zwiekszal za kazdym razem, 
        gdy uruchomi się petla lub zostanie wlaczony generator zegara 200hz
    */
    always @(posedge clk) begin
        if(clk_wyzw) begin
            clk_licznik = clk_licznik+1; 
        end
        if(clk_licznik == 3000) begin
            clk_200 = ~clk_200;
            clk_licznik = 0;
        end
    end
   
    //jeżeli jest rządanie piętra
    always @(zadane_pietro) begin
        clk_wyzw = 1; //wlaczenie wyzwalacza zegara
        clk_200 = ~clk_200; //negacja czestotliwosci zegara
        r_out_act_pietro <= in_act_pietro;
    end
    
    //praca windy
    always @(posedge clk) begin
    	//normalne działanie windy
        if(!rst && !nadczasowosc && !przeciazenie) begin
            //jedzie w górę
            if(zadane_pietro > r_out_act_pietro) begin
                r_kierunek = 1'b1; 
                r_out_act_pietro <= r_out_act_pietro << 1;
            end
            //jedzie w dół
            else if(zadane_pietro < r_out_act_pietro) begin
                r_kierunek = 1'b0;
                r_out_act_pietro = r_out_act_pietro >> 1;
            end
            //osiągnięcie żądanego piętra
            else if(zadane_pietro == r_out_act_pietro) begin
                r_koniec = 1;
                r_kierunek = 0;
            end
        end
        //gdy drzwi były otwarte dłużej niż założono
        else if(!rst && nadczasowosc) begin
            r_otwarte_drzwi = 1;
            r_koniec = 1;
            r_przeciazenie_pow = 0;
            r_kierunek = 0;
            r_out_act_pietro <= r_out_act_pietro;
        //winda nie pojedzie
        end
        //całkowita waga przekraczająca normę
        else if(!rst && przeciazenie) begin
            r_otwarte_drzwi = 0;
            r_przeciazenie_pow = 1;
            r_koniec = 1;
            r_kierunek = 0;
            r_out_act_pietro <= r_out_act_pietro;
        //winda nie pojedzie
        end
    end
endmodule