`timescale 1ns/1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: AMK INC
// Engineer: 
// 
// Create Date: 12.06.2018 05:43:24
// Design Name: 
// Module Name: elevator_controller_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module elevator_controller_tb;

	reg [5:0]zadane_pietro;
	reg [5:0]in_act_pietro;
	reg clk;
	reg rst;
	reg nadczasowosc;
	reg przeciazenie;
	wire kierunek;
	wire koniec;
	wire otwarte_drzwi;
	wire przeciazenie_pow;
	wire [5:0] out_act_pietro;

	//instancja windy 
	elevator_controller ect(
		.zadane_pietro(zadane_pietro),
		.in_act_pietro(in_act_pietro),
		.clk(clk),
		.rst(rst),
		.kierunek(kierunek),
		.out_act_pietro(out_act_pietro),
		.koniec(koniec),
		.nadczasowosc(nadczasowosc),
		.przeciazenie(przeciazenie),
		.otwarte_drzwi(otwarte_drzwi),
		.przeciazenie_pow(przeciazenie_pow)
		);

	//generowanie zegara i testowanie różnych wejść
	/*
	   1. z 7 pietra na parter
	   2. z parteru na 7 pietro
	*/
	initial
		begin
			#0 clk = 1'b0; rst = 1'b1; nadczasowosc = 1'b0; przeciazenie = 1'b0;
			#50 rst = 1'b0;
			#50 rst = 1'b1;
			#50 zadane_pietro = 6'b000001; in_act_pietro = 6'b100000;
			#50 rst = 1;
			#50 rst = 0;
			#50 zadane_pietro = 6'b100000; in_act_pietro = 6'b000001;
			#50 rst = 1'b1;
			#50 rst = 1'b0;
			#50 nadczasowosc = 1;
			#50 rst = 1'b1;
			#50 rst = 1'b0;
			#50 przeciazenie = 1;
			#50 rst = 1'b1;
		end
	   always #50 clk =~clk;
endmodule